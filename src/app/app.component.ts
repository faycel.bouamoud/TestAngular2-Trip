import { Component } from '@angular/core';
import { Trip } from './model/trip';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  trips: Trip[] = [{ from: 'Tunis', to: 'Djerba', seats: 53, time: 10 }];
  cities: string[] = ['Tunis', 'Nabeul', 'Djerba', 'Sousse'];
  from: string;
  to: string;
  seats: number;
  time: number;

  addCity(city: string) {
    this.cities.push(city);
  }

  log(o: object) {
    console.log(o);
  }

  addTrip(from: string, to:string, time: number, seats: number) {
    this.trips.push({ from: from, seats: seats, time: time, to: to });
  }
}
