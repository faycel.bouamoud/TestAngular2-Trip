import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

  transform(value: number): any {
    return Math.floor(value / 60) + 'hh : ' + value % 60 + 'mm';
  }

}
