export class Trip {
    from: string;
    to: string;
    time: number;
    seats: number;

    constructor() {
    }
}
