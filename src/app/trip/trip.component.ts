import { Trip } from './../model/trip';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-trip',
  templateUrl: './trip.component.html',
  styleUrls: ['./trip.component.css']
})
export class TripComponent {
  @Input()
  trip: Trip ;
  constructor() { }
  book() {
    if (this.trip.seats > 0) {
      this.trip.seats -= 1;
    }
  }
}
